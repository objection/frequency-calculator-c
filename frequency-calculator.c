#define _ISOC99_SOURCE
#define CAME_FROM_C 1
#include "frequency-calculator.h"
#include <math.h>

// Justification for this. tgmath.h doesn't seem to work. I want to
// uses the float versions and it seems to be using the double versions.
// Regardless I'm getting the wrong answers. This could be a typedef, probably.
// Couldn't get that to work. So these are macros.
#if defined FC_USE_FLOAT
#define FC_CEIL ceilf
#define FC_POW powf
#define FC_FLOOR floorf
#else
#define FC_CEIL ceil
#define FC_POW pow
#define FC_FLOOR floor
#endif

/**
 * The amount of half steps a note is from A
 * @type {Object}
 */

#define x(a, b, c) b,
char *note_strs[] = {STEPS};
#undef x
#define x(a, b, c) c,
int steps[] = {STEPS};
#undef x


/**
 * Calculate the amount of half steps between A4 and a given note and octave
 * @param  {String} note   The note
 * @param  {Number} octave The octave
 * @return {Number}        The number of half steps
 */
FC_FLOAT fc_steps (enum fc_note_name name, FC_FLOAT octave) {
	return ((4.0 - octave) * -12.0) + steps[name];
}

/**
 * Calculate the frequency of a note based on the amount of half steps
 * above or below the base note (A4).
 * This can be a positive number (above) or a negative number (below).
 * @param  {Number} steps The number of half steps
 * @return {Number}       The calculated frequency
 */
FC_FLOAT fc_frequency_by_step (FC_FLOAT steps, FC_FLOAT base) {
	return base * FC_POW (FC_POW (2.0, (1.0 / 12.0)), steps);
}

/**
 * Calculate the frequency of a note based on the note and octave
 * @param  {String} note   The note
 * @param  {Number} octave The octave
 * @return {Number}        The frequency of the note
 */
FC_FLOAT fc_frequency_by_note (enum fc_note_name name, FC_FLOAT octave,
		FC_FLOAT base) {
	return fc_frequency_by_step (fc_steps (name,
				octave), base);
}

/**
 * Calculate the amount of half steps between A4 and a given frequency
 * @param  {Float}   frequency The frequency
 * @param  {Boolean} round     Should the steps be rounded
 * @return {Number}            The amount of half steps
 */
FC_FLOAT fc_steps_from_frequency (FC_FLOAT frequency, FC_FLOAT base,
		bool _round) {
	FC_FLOAT steps = 12.0 * log (frequency / base) / log (2.0);

	if (_round) {
		return round (steps);
	}

	return steps;
}

/**
 * Calculate the note by the distance of half steps from A4
 * @param  {Number} steps The amount of half steps from A4
 * @return {String}       The note
 */
char *fc_note_by_steps (FC_FLOAT steps) {
	FC_FLOAT octave = steps / 12.0;
	int s = steps - ((steps < 0.0? FC_CEIL (octave): FC_FLOOR (octave)) *
			12.0);
	// NOTE: here are hacks. Maybe Javascript arrays wrap around.
	if (s == 11 || s <= -10) s += 12;
	if (s + 9 > 11)
		while (s > 0) s -= 12;


	return note_strs[s + 9];
}

/**
 * Calculate the octave by the distance of half steps from A4
 * @param  {Number}  steps        The amount of half steps from A4
 * @param  {Boolean} relativeToA4 Should the octave be relative to A4
 * @return {Number}               The octave
 */
int fc_octave_by_steps (FC_FLOAT steps, bool relativeToA4) {
	FC_FLOAT octave = steps / 12.0;

	if (relativeToA4)
		return steps < 0.0? FC_CEIL (octave): FC_FLOOR (octave);

	return (steps < 0.0? FC_CEIL (octave): FC_FLOOR (octave)) + 4.0;
}

/**
 * Calculate the note of a given frequency
 * @param  {Number} frequency The frequency of a note
 * @return {String}           The note
 */
char *fc_note_by_frequency (FC_FLOAT frequency, FC_FLOAT base) {
	return fc_note_by_steps (fc_steps_from_frequency
			(frequency, base, 1));
}

/**
 * Calculate the octave of a frequency
 * @param  {Number}  frequency    The frequency of an octave
 * @param  {Boolean} relativeToA4 Should the octave be relative to A4
 * @return {Number}               The octave
 */
int fc_octave_by_frequency (FC_FLOAT frequency, FC_FLOAT base,
		bool relativeToA4) {
	return fc_octave_by_steps (fc_steps_from_frequency
			(frequency, base, 0), relativeToA4);
}
