/**
 * Author: Paul
 *
 * Author: Sam Bellen
 * Github: https://github.com/Sambego/frequency-calculator
 * Twitter: @Sambego
 *
 * This is a straight rendering into C of frequency-calculator by Sam Bellen.
 *
 * I've used a macro, FC_FLOAT, for the float size. It's set to actual floats
 * but you can change it to doubles.
 *
 * I've shorted the names: fc_calculate_frequency_by_note becomes
 * fc_frequency_by_note.
 *
 * This is a small library to convert frequencies to steps, notes and octaves.
 *
 * Examples:
 *
 * Convert a note to a frequency:
 * fc_frequency_by_note (A, 4); // -> 440
 *
 * Convert a frequency to a note:
 * fc_note_by_frequency (440); // -> A
 *
 * Convert a frequency to an octave:
 * fc_octave_by_frequency (440); // -> 0
 */

#define FC_USE_FLOAT 1
#include <stdbool.h>
#if defined FC_USE_FLOAT
#define FC_FLOAT float
#else
#define FC_FLOAT double
#endif

// I've called the white notes "C-", etc because I'm using this lib in my tracker,
// and trackers display notes like this: "C-5", "C♯5".
//
// If you want other strings, just make your changes here. This means forking, of course.
// But this lib is not likely to change, anyway.
#define STEPS \
    x(FCN_C, "C-", -9) \
    x(FCN_C_SHARP, "C♯", -8) \
    x(FCN_D, "D-", -7) \
    x(FCN_D_SHARP, "D♯", -6) \
    x(FCN_E, "E-", -5) \
    x(FCN_F, "F-", -4) \
    x(FCN_F_SHARP, "F♯", -3) \
    x(FCN_G, "G-", -2) \
    x(FCN_G_SHARP, "G♯", -1) \
    x(FCN_A, "A-", 0) \
    x(FCN_A_SHARP, "A♯", 1) \
    x(FCN_B, "B-", 2)


#define x(a, b, c) a,
enum fc_note_name {STEPS};
#undef x

FC_FLOAT fc_steps (enum fc_note_name name, FC_FLOAT octave);
FC_FLOAT fc_frequency_by_step (FC_FLOAT steps, FC_FLOAT base);
FC_FLOAT fc_frequency_by_note (enum fc_note_name name, FC_FLOAT octave,
		FC_FLOAT base);
FC_FLOAT fc_steps_from_frequency (FC_FLOAT frequency, FC_FLOAT base,
		bool _round);
char *fc_note_by_steps (FC_FLOAT steps);
int fc_octave_by_steps (FC_FLOAT steps, bool relativeToA4);
char *fc_note_by_frequency (FC_FLOAT frequency, FC_FLOAT base);
int fc_octave_by_frequency (FC_FLOAT frequency, FC_FLOAT base,
		bool relativeToA4);
